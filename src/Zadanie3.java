
import java.util.Scanner;

public class Zadanie3 {



    public static void main(String[] args) {
        int[] list = new int[10001];
        for (int i = 0; i <= 10000; i++) {
            list[i] = i;
        }

        Scanner scanner = new Scanner(System.in);
        System.out.print("Write a number up to 10000: ");
        int number = scanner.nextInt();
        System.out.println(indexOfNumber(list, 0, list.length, number));


    }

    private static int indexOfNumber(int [] list, int left, int right, int number) {
        if (left < right) {
            int centre = (left + right)/2;
            if (list[centre] == number) {
                return centre;
            } else if (list[centre] > number) {
                return indexOfNumber(list, 0 , centre, number);
            } else if (list[centre] < number) {
                return indexOfNumber(list,centre+1,right,number);
            }
        }

        return -1;
    }

}