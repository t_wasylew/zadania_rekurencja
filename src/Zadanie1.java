public class Zadanie1 {

    private int indeks;
    private int podstawa;

    public int potega(int podstawa, int indeks) {
        if (podstawa == 0) {
            return 1;
        }else {
            return podstawa * potega(podstawa, --indeks);
        }
    }
}
